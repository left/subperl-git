#
# Przyklad nr 2
#

$r = 2;

$PI = 3.14159;

$length = 2 * $PI * $r;
$area = $PI * $r ** 2;

if($length > 12 and $area < 12) {
	print("1 Dlugi okrag, male kolo!\n");
}
elsif($length > 12 and $area > 12) {
	print("2 Dlugi okrag, duze kolo!\n");
}
elsif($length < 12 and $area < 12) {
	print("3 Krotki okrag, male kolo!\n");
}
else {
	print("4 Krotki okrag, duze kolo!\n");
}

print($length);
print("\n");
print($area);
print("\n");
