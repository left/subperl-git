#!/usr/bin/perl -w

$subperl = "./../../bin/subperl";
$perl = "perl";
$tests = "../*.pl";

@files = glob $tests;
foreach $file (@files) {
  $perl_out = `$perl $file`;
  $subperl_out = `$subperl $file`;
  if($perl_out ne $subperl_out) {
  	print "Blad dla pliku: ".$file."\n";
  	print "\n";
  	print $subperl_out;
  	print "\n";
  	print $perl_out;
  	print "\n";
  	print "\n"; 
  }
}
