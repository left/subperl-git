#
# Regexp test
#

$str = "abba";

if($str =~ "a(bb)+ac?") {
	print("OK!");
}
else {
	print("WHOOPS!?");
}


$str = "Ala ma kota, kot ma Ale";

if($str =~ "(Ala)? ma (kot|,| )*a (ma| )+ Ale") {
	print("OK!");
}
else {
	print("WHOOPS!?");
}


print("\n");