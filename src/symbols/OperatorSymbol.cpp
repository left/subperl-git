#include "OperatorSymbol.hpp"

OperatorSymbol::OperatorSymbol(const atoms::Atom& optype) : Symbol(OPERATOR), type(optype) {
	/* empty */
}

OperatorSymbol::~OperatorSymbol() { /* empty */ }

atoms::Atom OperatorSymbol::getOperatorType() const {
	return type;
}

bool OperatorSymbol::isUnary() const {
	switch(type) {
		case atoms::UNARY_MINUS:
		case atoms::UNARY_PLUS:
		case atoms::NOT:
			return true;
		default:
			return false;
	}
}
