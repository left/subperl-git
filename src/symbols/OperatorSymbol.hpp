#ifndef OPERATORSYMBOL_HPP_
#define OPERATORSYMBOL_HPP_

#include "Symbol.hpp"
#include <boost/shared_ptr.hpp>

class OperatorSymbol;

typedef boost::shared_ptr<OperatorSymbol> POperatorSymbol;

/**
 * Reprezenuje operator w czasie wykonania.
 */
class OperatorSymbol: public Symbol {
	private:
		/** Typ operatora. */
		atoms::Atom type;
	public:
		/** Konstruktor ustawia typ operatora. */
		OperatorSymbol(const atoms::Atom& optype);
		virtual ~OperatorSymbol();

		/**
		 * @return Zwraca typ operatora.
		 */
		atoms::Atom getOperatorType() const;

		bool isUnary() const;
};

#endif /* OPERATORSYMBOL_HPP_ */
