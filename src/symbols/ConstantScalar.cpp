#include "ConstantScalar.hpp"

#include <boost/lexical_cast.hpp>

ConstantScalar::ConstantScalar(const Type& type) : Symbol(type) { /* empty */ }

ConstantScalar::ConstantScalar(const int& value) : Symbol(CONSTANT) {
	intValue = value;
	valueType = 1;
}

ConstantScalar::ConstantScalar(const float& value) : Symbol(CONSTANT) {
	floatValue = value;
	valueType = 0;
}

ConstantScalar::ConstantScalar(const std::string& value) : Symbol(CONSTANT) {
	stringValue = value;
	valueType = 2;
}

ConstantScalar::ConstantScalar(const ConstantScalar& scalar) : Symbol(CONSTANT) {
	valueType = scalar.valueType;
	floatValue = scalar.floatValue;
	intValue = scalar.intValue;
	stringValue = scalar.stringValue;
}

ConstantScalar::~ConstantScalar() { /* empty */ }

bool ConstantScalar::isTrue() const {
	switch(valueType) {
		case 0:
			return floatValue != 0.0f ? true : false ;
		case 1:
			return intValue != 0 ? true : false;
		case 2:
			return stringValue.empty() ? false : true;
	}
	return true;
}

float ConstantScalar::getFloatValue() const {
	switch(valueType) {
		case 0:
			return floatValue;
		case 1:
			return static_cast<float>(intValue);
		case 2:
			return boost::lexical_cast<float>(parseForFloat(stringValue));
	}
	return 0.0f;
}

int ConstantScalar::getIntValue() const {
	switch(valueType) {
		case 0:
			return static_cast<int>(floatValue);
		case 1:
			return intValue;
		case 2:
			return boost::lexical_cast<int>(parseForInteger(stringValue));
	}
	return 0;
}

std::string ConstantScalar::getStringValue() const {
	switch(valueType) {
		case 0:
			return boost::lexical_cast<std::string>(floatValue);
		case 1:
			return boost::lexical_cast<std::string>(intValue);
		case 2:
			return stringValue;
	}
	return std::string();
}

std::string ConstantScalar::parseForInteger(const std::string& str) {
	std::string result;
	std::string::const_iterator it = str.begin();

	bool isNumber = false;

	if(*it == '-' || *it == '+') {
		result.push_back(*it);
		++it;
	}

	for(; it != str.end(); ++it) {
		if(!isdigit(*it)) {
			break;
		}
		result.push_back(*it);
		isNumber = true;
	}

	if(isNumber) {
		return result;
	}
	return std::string("0");
}

std::string ConstantScalar::parseForFloat(const std::string& str) {
	std::string result;
	std::string::const_iterator it = str.begin();
	bool dot = false;
	bool isNumber = false;

	if(*it == '-' || *it == '+') {
		result.push_back(*it);
		++it;
	}

	for(; it != str.end(); ++it) {
		if(*it == '.') {
			if(!dot) {
				dot = true;
			}
			else {
				break;
			}
		}
		else if(!isdigit(*it)) {
			break;
		}
		result.push_back(*it);
		isNumber = true;
	}

	if(isNumber) {
		return result;
	}
	return std::string("0");
}
