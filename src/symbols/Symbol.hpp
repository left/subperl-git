#ifndef SYMBOL_HPP_
#define SYMBOL_HPP_

#include "../atoms/Atom.hpp"

#include <boost/shared_ptr.hpp>

class Symbol;

typedef boost::shared_ptr<Symbol> PSymbol;

/**
 * Klasa abstrakcyjna reprezenujaca nadtyp dla elementow tablic symboli.
 */
class Symbol {
	public:
		/**
		 * Enumeracja ta pozwala rozroznic typ.
		 * Hashe i listy nie sa wspierane.
		 */
		enum Type {
			SCALAR, //!< Mowi, ze symbol jest skalarem.
			CONSTANT, //!< Mowi ze symbol jest stala
			OPERATOR //!< Mowi ze symbol jest operatorem
		};

	private:
		/** Typ symbolu. */
		Type type;

	protected:
		/**
		 * Klasa abstrakcyjna.
		 *
		 * @param t Type symbolu.
		 */
		Symbol(const Type& t) : type(t) {}

	public:
		virtual ~Symbol() {}

		/**
		 * @return Typ symbolu.
		 */
		Type getType() const { return type; }
};

#endif /* SYMBOL_HPP_ */
