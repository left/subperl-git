#include "Scalar.hpp"

Scalar::Scalar() : ConstantScalar(SCALAR) {
	valueType = 1;
	intValue = 0;
}

Scalar::~Scalar() { /* empty */}

void Scalar::setValue(const ConstantScalar& scalar) {
	if(scalar.isFloat()) {
		floatValue = scalar.getFloatValue();
		valueType = 0;
	}
	else if(scalar.isInteger()) {
		intValue = scalar.getIntValue();
		valueType = 1;
	}
	else if(scalar.isString()) {
		stringValue = scalar.getStringValue();
		valueType = 2;
	}
}
