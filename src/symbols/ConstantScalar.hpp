#ifndef CONSTANTSCALAR_HPP_
#define CONSTANTSCALAR_HPP_

#include "Symbol.hpp"

class ConstantScalar;

typedef boost::shared_ptr<ConstantScalar> PConstantScalar;

/**
 * Klasa reprezentuje staly skalar perlowy.
 * Udostepnia przezroczyste rzutowanie typow.
 */
class ConstantScalar: public Symbol {
	private:
		/**
		 * Parsuje w specyficzny dla perla sposob stringa aby moc go zrzutowac na int.
		 */
		static std::string parseForInteger(const std::string& str);
		/**
		 * Parsuje w specyficzny dla perla sposob stringa aby moc go zrzutowac na float.
		 */
		static std::string parseForFloat(const std::string& str);

	protected:
		/**	Wartosc rzeczywista. */
		float floatValue;
		/**	Wartosc calkowita. */
		int intValue;
		/**	Wartosc lancuchowa. */
		std::string stringValue;

		/**
		 * Typ wartosci skalara: 0-float, 1-int, 2-string.
		 */
		int valueType;

		/**	Konstruktor dla klas dziedziczacych. */
		ConstantScalar(const Type& type);

	public:
		ConstantScalar(const int& value);
		ConstantScalar(const float& value);
		ConstantScalar(const std::string& value);
		ConstantScalar(const ConstantScalar& scalar);
		virtual ~ConstantScalar();

		bool isFloat() const { return valueType == 0; }
		bool isInteger() const { return valueType == 1; }
		bool isString() const { return valueType == 2; }

		bool isTrue() const;

		float getFloatValue() const;
		int getIntValue() const;
		std::string getStringValue() const;

};

#endif /* CONSTANTSCALAR_HPP_ */
