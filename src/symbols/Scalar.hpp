#ifndef SCALAR_HPP_
#define SCALAR_HPP_

#include "ConstantScalar.hpp"

class Scalar;

typedef boost::shared_ptr<Scalar> PScalar;

/**
 * Klasa reprezentuje zmienna skalarna.
 * Dziedziczy wszystkie cechy po stalej skalarnej.
 * Dodatkowo mozna ustawic jej wartosc.
 */
class Scalar: public ConstantScalar {
	public:
		Scalar();
		~Scalar();

		/**
		 * Ustawia wartosc na podana w parametrze.
		 */
		void setValue(const ConstantScalar& scalar);
};

#endif /* SCALAR_HPP_ */
