#include "Parser.hpp"

#include <iostream>
#include "utils/Trace.hpp"

Parser::Parser(Scanner& _scanner, Calculator& _calculator ) : scanner(_scanner), calculator(_calculator) {

	using namespace atoms;
	arithmeticFirst = AtomsSet(PLUS, MINUS, BRACKET_LEFT, INTEGER, FLOAT, QUOTED, DOUBLE_QUOTED, IDENTIFIER, END);
	comparisonFirst = arithmeticFirst;
	orTestFirst = comparisonFirst + AtomsSet(NOT);
	statementFirst = orTestFirst + AtomsSet(PRINT, IF, END);
	comparisonOperators = AtomsSet(LESS, LESS_EQUAL, EQUAL, GREATER, GREATER_EQUAL, REGEXP_MATCH, NOT_EQUAL, END);
	addOperators = AtomsSet(PLUS, MINUS, END);
	mulOperators = AtomsSet(MUL, DIV , MOD, END);
	unaryOperators = addOperators;

	previousAtom = END; // sztuczny znacznik
	execution = true;
	// pobranie 1-szego atomu
}

Parser::~Parser() { /* empty body */ }

void Parser::prepare() {
	scanner.prepare();
	getNextAtom();
}

void Parser::getNextAtom() {
	if(previousAtom == atoms::END) {
		currentAtom = scanner.getNextAtom();
	}
	else {
		currentAtom = previousAtom;
		previousAtom = atoms::END;
	}
}

void Parser::ungetAtom(const atoms::Atom& atom) {
	previousAtom = currentAtom;
	currentAtom = atom;
}

void Parser::accept(const atoms::Atom& atom) {
	if(atom == currentAtom) {
		getNextAtom();
	}
	else {
		throw ParserException(scanner.getTextPosition(), currentAtom, AtomsSet(atom));
	}
}

void Parser::execute() {
	execution = true;
}

void Parser::stopExecute() {
	execution = false;
}

PConstantScalar Parser::calculate() {
	if(execution) {
		return calculator.calculate();
	}
	else {
		return PConstantScalar();
	}
}

void Parser::addScalar(const PSymbol& scalar) {
	if(execution) {
		return calculator.appendSymbol(scalar);
	}
}

void Parser::addOperator(const POperatorSymbol& op) {
	if(execution) {
		return calculator.appendOperator(op);
	}
}

void Parser::program() throw(ParserException, DivisionByZero, RegexpException, ScannerException) {
	Trace t("program");
	while(statementFirst.has(currentAtom)) {
		statement();
	}
	if(currentAtom != atoms::EOT) {
		throw ParserException(scanner.getTextPosition(), currentAtom, AtomsSet(atoms::EOT));
	}
}

void Parser::statement() {
	Trace t("statement");
	if(currentAtom == atoms::IF) {
		ifStatement();
	}
	else {
		instruction();
		accept(atoms::SEMICOLON);
	}

}

void Parser::instruction() {
	Trace t("instruction");
	if(currentAtom == atoms::PRINT) {
		printStatement();
	}
	else if(orTestFirst.has(currentAtom)) {
		orTest();
		calculate();
	}
	else {
		throw ParserException(scanner.getTextPosition(), currentAtom, orTestFirst + AtomsSet(atoms::PRINT));
	}
}

void Parser::statementBlock() {
	Trace t("statementBlock");
	accept(atoms::SCOPE_BEGIN);
	while(statementFirst.has(currentAtom)) {
		statement();
	}
	if(currentAtom == atoms::SCOPE_END) {
		accept(atoms::SCOPE_END);
	}
	else {
		throw ParserException(scanner.getTextPosition(), currentAtom, statementFirst + AtomsSet(atoms::SCOPE_END));
	}
}

void Parser::printStatement() {
	Trace t("printStatement");
	accept(atoms::PRINT);
	accept(atoms::BRACKET_LEFT);

	orTest();
	PConstantScalar scalar = calculate();
	if(scalar.get() != NULL) {
		std::cout << scalar->getStringValue();
	}

	accept(atoms::BRACKET_RIGHT);
}

void Parser::ifStatement() {
	Trace t("ifStatement");
	bool execeuteIf;

	execeuteIf = ifExpression();
	statementBlock();

	if(execeuteIf == true) {
		stopExecute();
	}
	else {
		execute(); // jezeli byl falsz to pozwol liczyc inne warunki
	}

	while(currentAtom == atoms::ELSIF) {
		execeuteIf = elsifExpression();
		statementBlock();

		if(execeuteIf == true) {
			stopExecute();
		}
		else {
			execute(); // jezeli byl falsz to pozwol liczyc inne warunki
		}
	}
	if(currentAtom == atoms::ELSE) {
		elseExpression();
		statementBlock();
	}

	execute(); // pozwol liczyc
}

bool Parser::ifExpression() {
	Trace t("ifExpression");
	bool ret = true; // to wazne w ifStatement() - haxoza
	accept(atoms::IF);
	accept(atoms::BRACKET_LEFT);
	orTest();
	PConstantScalar scalar = calculate();
	if(scalar.get() != NULL) {
		if(scalar->isTrue()) {
			execute();
			ret = true;
		}
		else {
			stopExecute();
			ret = false;
		}
	}
	accept(atoms::BRACKET_RIGHT);
	return ret;
}

bool Parser::elsifExpression() {
	Trace t("elsifExpression");
	bool ret = true; // to wazne w ifStatement() - haxoza
	accept(atoms::ELSIF);
	accept(atoms::BRACKET_LEFT);
	orTest();
	PConstantScalar scalar = calculate();
	if(scalar.get() != NULL) {
		if(scalar->isTrue()) {
			execute();
			ret = true;
		}
		else {
			stopExecute();
			ret = false;
		}
	}
	accept(atoms::BRACKET_RIGHT);
	return ret;
}

void Parser::elseExpression() {
	Trace t("elseExpression");
	accept(atoms::ELSE);
}

void Parser::orTest() {
	Trace t("orTest");
	andTest();
	if(currentAtom == atoms::OR) {
		accept(atoms::OR);
		addOperator(POperatorSymbol(new OperatorSymbol(atoms::OR)));
		orTest();
	}
}

void Parser::andTest() {
	Trace t("andTest");
	notTest();
	if(currentAtom == atoms::AND) {
		accept(atoms::AND);
		addOperator(POperatorSymbol(new OperatorSymbol(atoms::AND)));
		andTest();
	}
}

void Parser::notTest() {
	Trace t("notTest");
	if(currentAtom == atoms::NOT) {
		accept(atoms::NOT);
		addOperator(POperatorSymbol(new OperatorSymbol(atoms::NOT)));
		notTest();
	}
	else if(comparisonFirst.has(currentAtom)) {
		comparison();
	}
	else {
		throw ParserException(scanner.getTextPosition(), currentAtom, comparisonFirst + AtomsSet(atoms::NOT));
	}
}

void Parser::comparison() {
	Trace t("comparison");
	bool isAssignment = false;
	bool doUnget = false;

	if(currentAtom == atoms::IDENTIFIER) {
		getNextAtom();
		doUnget = true;
		if(currentAtom == atoms::ASSIGNMENT) {
			ungetAtom(atoms::IDENTIFIER);

			assignment();
			isAssignment = true;
		}
	}

	if(!isAssignment) {
		if(doUnget) {
			ungetAtom(atoms::IDENTIFIER);
		}

		arithmeticExpression();
		if(comparisonOperators.has(currentAtom)) {
			addOperator(POperatorSymbol(new OperatorSymbol(currentAtom)));
			accept(currentAtom); // operator comparison
			arithmeticExpression();
		}
	}
}

void Parser::arithmeticExpression() {
	Trace t("arithmeticExpression");
	mulExpression();
	if(addOperators.has(currentAtom)) {
		addOperator(POperatorSymbol(new OperatorSymbol(currentAtom)));
		accept(currentAtom);
		arithmeticExpression();
	}
}

void Parser::mulExpression() {
	Trace t("mulExpression");
	unaryExpression();
	if(mulOperators.has(currentAtom)) {
		addOperator(POperatorSymbol(new OperatorSymbol(currentAtom)));
		accept(currentAtom);
		mulExpression();
	}
}

void Parser::unaryExpression() {
	Trace t("unaryExpression");
	if(currentAtom == atoms::PLUS) {
		accept(atoms::PLUS);
		addOperator(POperatorSymbol(new OperatorSymbol(atoms::UNARY_PLUS)));
		unaryExpression();
	}
	else if(currentAtom == atoms::MINUS) {
		accept(atoms::MINUS);
		addOperator(POperatorSymbol(new OperatorSymbol(atoms::UNARY_MINUS)));
		unaryExpression();
	}
	else {
		powerExpression();
	}
}

void Parser::powerExpression() {
	Trace t("powerExpression");
	baseExpression();
	if(currentAtom == atoms::POWER) {
		accept(atoms::POWER);
		addOperator(POperatorSymbol(new OperatorSymbol(atoms::POWER)));
		powerExpression();
	}
}

void Parser::baseExpression() {
	Trace t("baseExpression");
	if(currentAtom == atoms::BRACKET_LEFT) {
		accept(atoms::BRACKET_LEFT);
		addOperator(POperatorSymbol(new OperatorSymbol(atoms::BRACKET_LEFT)));
		orTest();
		accept(atoms::BRACKET_RIGHT);
		addOperator(POperatorSymbol(new OperatorSymbol(atoms::BRACKET_RIGHT)));
	}
	else if(currentAtom == atoms::IDENTIFIER) {
		accept(atoms::IDENTIFIER);
		addScalar(scanner.getLastSymbol());
	}
	else if(currentAtom == atoms::INTEGER) {
		accept(atoms::INTEGER);
		addScalar(scanner.getLastConstant());
	}
	else if(currentAtom == atoms::FLOAT) {
		accept(atoms::FLOAT);
		addScalar(scanner.getLastConstant());
	}
	else if(currentAtom == atoms::QUOTED || currentAtom == atoms::DOUBLE_QUOTED) {
		accept(currentAtom);
		addScalar(scanner.getLastConstant());
	}
	else {
		AtomsSet baseExpressionFirst(atoms::BRACKET_LEFT, atoms::IDENTIFIER,
				atoms::INTEGER, atoms::FLOAT, atoms::QUOTED, atoms::DOUBLE_QUOTED, atoms::END);
		throw ParserException(scanner.getTextPosition(), currentAtom, baseExpressionFirst);
	}
}

void Parser::assignment() {
	Trace t("assignment");
	if(currentAtom == atoms::IDENTIFIER) {
		accept(atoms::IDENTIFIER);
		PSymbol storedSymbol = scanner.getLastSymbol(); // haxoza
		accept(atoms::ASSIGNMENT);

		// dirty hack - assignment should be calculate form right to left
		// code below simulate this concept
		orTest();
		PConstantScalar result = calculate();
		addScalar(storedSymbol);
		addOperator(POperatorSymbol(new OperatorSymbol(atoms::ASSIGNMENT)));
		addScalar(result);
	}
	else {
		throw ParserException(scanner.getTextPosition(), currentAtom, AtomsSet(atoms::IDENTIFIER));
	}
}
