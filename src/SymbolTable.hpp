#ifndef SYMBOLTABLE_HPP_
#define SYMBOLTABLE_HPP_

#include "symbols/Symbol.hpp"

#include <string>
#include <boost/unordered_map.hpp>

/**
 * Klasa reprezentujaca tablice symboli.
 *
 */
class SymbolTable {
	private:
		typedef boost::unordered_map<std::string, PSymbol> StringToSymbolMap;

		/** Mapa nazw symboli na ich obiekty. */
		StringToSymbolMap strToSymbolMap;

	public:
		/**
		 * K-tor
		 */
		SymbolTable();

		/**
		 * D-tor
		 */
		~SymbolTable();

		/**
		 * @return True jesli tablica zawiera symbol o podanej nazwie.
		 */
		bool hasName(const std::string& name) const;

		/**
		 * Rejestruje podany symbol w tablicy, jezeli dana nazwa symbolu
		 * jest juz zarejestrowana to nie robi nic.
		 *
		 * @param name Nazwa symbolu
		 * @param symbol Wskaznik na symbol
		 */
		void registerSymbol(const std::string& name, const PSymbol& symbol);

		/**
		 * @param name Nazwa symbolu ktory ma byc zworcony.
		 * @return Wskazanie na symbol o podanej nazwie.
		 */
		PSymbol getSymbol(const std::string&name) const;

};

#endif /* SYMBOLTABLE_HPP_ */
