#include "TextPosition.hpp"

#include <sstream>

TextPosition::TextPosition(const std::string& f, const int& c, const int& l) : column(c), line(l), fileName(f) {
	/* empty body */
}

std::string TextPosition::toString() const {
	std::stringstream msg;
	msg << fileName << ":" << line << ":" << column;
	return msg.str();
}

int TextPosition::getColumn() const {
	return column;
}

int TextPosition::getLine() const {
	return line;
}

std::string TextPosition::getFileName() const {
	return fileName;
}
