#ifndef TEXTPOSITION_HPP_
#define TEXTPOSITION_HPP_

#include <string>

/**
 * Klasa reprezenruje pozycje w tekscie.
 */
class TextPosition {
	private:
		/** Pozycja w wierszu liczona od 1. */
		int column;
		/** Numer linii w tekscie liczony od 1. */
		int line;
		/** Nazwa pliku (opcjonalnie) */
		std::string fileName;

	public:
		/**
		 * @param c Pozycja w wierszu
		 * @param l Numer lini
		 * @param f Nazwa pliku
		 */
		TextPosition(const std::string& f = std::string(""), const int& c = 1, const int& l = 1);

		/**
		 * @return Zwraca string nadajacy sie do komunikatu o bledzie.
		 */
		std::string toString() const;

		int getColumn() const;
		int getLine() const;
		std::string getFileName() const;

		inline void incColunm();
		inline void incLine();
};


void TextPosition::incColunm(){
	++column;
}

void TextPosition::incLine() {
	column = 1;
	++line;
}

#endif /* TEXTPOSITION_HPP_ */
