#ifndef TRACE_HPP_
#define TRACE_HPP_

#include <string>

/**
 * Klasa do sledzenia wywolan procedur rozbioru.
 */
class Trace {
	private:
		static bool traceOn;
		static int level;

		std::string methodName;

	public:
		Trace(const std::string& methodName);
		~Trace();

		inline static void trace(bool v) { traceOn = v; }
};

#endif /* TRACE_HPP_ */
