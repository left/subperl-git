#include "Trace.hpp"

#include <iostream>

bool Trace::traceOn = false;
int Trace::level = 0;

Trace::Trace(const std::string& name) {
	if(traceOn) {
		methodName = name;
		std::cout << "Wchodze do: " << name << std::endl;
		++level;
	}
}

Trace::~Trace() {
	if(traceOn) {
		--level;
		std::cout << "Wychodze z: " << methodName << std::endl;
	}
}
