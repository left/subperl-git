#ifndef PARSER_HPP_
#define PARSER_HPP_

#include "Scanner.hpp"
#include "atoms/AtomsSet.hpp"
#include "exceptions/ParserException.hpp"
#include "Calculator.hpp"

/**
 * Klasa reprezentujaca analizator skladnowy.
 *
 * Pobiera kolejne atomy leksyklane od skanera.
 */
class Parser {
	private:
		/** Referencja na analizator leksykalny podajacy kolejne atomy. */
		Scanner& scanner;
		/** Obiekt obliczajacy wyrazenia perlowe. */
		Calculator& calculator;

		/** Ostatnio wczytany atom ze skanera. */
		atoms::Atom currentAtom;
		/** Zmienna pomocnicza do zapamietywania atomu jezeli chcemy sie cofnac o jeden. */
		atoms::Atom previousAtom;

		bool execution;

		/** Zbior First dla procedury rozbioru statement(). */
		AtomsSet statementFirst;
		/** Zbior First dla procedury rozbioru orTest(). */
		AtomsSet orTestFirst;
		/** Zbior First dla procedury rozbioru comparison(). */
		AtomsSet comparisonFirst;
		/** Zbior First dla procedury rozbioru arithmeticExpresion(). */
		AtomsSet arithmeticFirst;
		/** Operatory porownania. */
		AtomsSet comparisonOperators;
		/** Operatory addytywne. */
		AtomsSet addOperators;
		/** Operatory multiplikatywne. */
		AtomsSet mulOperators;
		/** Operatory unarne. */
		AtomsSet unaryOperators;

		/**
		 * Zapisuje do zmiennej atom pobrany ze skanera.
		 */
		void getNextAtom();

		/**
		 * Ustawia currentAtom na podany w parametrze, zapisujac go wczesniej do previousAtom.
		 * @param atom Atom do ustawienia.
		 */
		void ungetAtom(const atoms::Atom& atom);

		/**
		 * Sprawdza czy aktualny atom zgadza sie z podanym w parametrze.
		 * Pobiera nastepny atom.
		 *
		 * @param atom Atom z ktorym zostanie porownany aktualnie wczytany atom.
		 */
		void accept(const atoms::Atom& atom);

		/**
		 * Ustawia flagi parsera na tryb wykonywania.
		 */
		void execute();

		/**
		 * Ustawia flagi parsera na tryb samej analizy skladniowej.
		 */
		void stopExecute();

		/**
		 * Metoda obudowuje Calculator uwzgledniajac flagi parsera dla wykonywania.
		 */
		PConstantScalar calculate();

		/**
  	  	 * Metoda obudowuje Calculator uwzgledniajac flagi parsera dla wykonywania.
		 */
		void addScalar(const PSymbol& scalar);

		/**
		 * Metoda obudowuje Calculator uwzgledniajac flagi parsera dla wykonywania.
		 */
		void addOperator(const POperatorSymbol& op);



		/* Procedury rozbioru. */
		void statement();
		void statementBlock();
		void instruction();

		void ifStatement();
		bool ifExpression();
		bool elsifExpression();
		void elseExpression();

		void printStatement();

		void orTest();
		void andTest();
		void notTest();
		void comparison();

		void assignment();
		void arithmeticExpression();
		void mulExpression();
		void unaryExpression();
		void powerExpression();
		void baseExpression();
		/* Koniec deklaracji procedur rozbioru. */

	public:
		/**
		 * K-tor inicjalizuje zwiazany z parserem skaner oraz pobiera pierwszy symbol.
		 *
		 * @param scanner Skaner z ktorego parser bedzie pobieral dane.
		 */
		Parser(Scanner& scanner, Calculator& calculator);
		/**
		 * D-tor
		 */
		~Parser();

		/**
		 * Glowna procedura rozbioru.
		 */
		void program() throw(ParserException, DivisionByZero, RegexpException, ScannerException);

		/**
		 * Metoda przygotowuje parser do uzycia.
		 * Pobiera pierwszy atom.
		 */
		void prepare();
};

#endif /* PARSER_HPP_ */
