#ifndef NFABUILDER_HPP_
#define NFABUILDER_HPP_

#include "Nfa.hpp"
#include "../exceptions/RegexpException.hpp"

#include <stack>

/**
 * Budowniczy automatu skonczonego.
 */
class NfaBuilder {
	private:
		/** Stos fragmentow automatu. */
		std::stack<Nfa> fragmentStack;

		/**
		 * Sprawdza czy stos ma podana wysokosc, jesli nie to rzuca wyjatek.
		 */
		void checkStack(unsigned int size) throw(RegexpException);

	public:
		NfaBuilder();
		~NfaBuilder();

		void addChar(char c) throw(RegexpException);
		void addConcatenation() throw(RegexpException);
		void addAlternative() throw(RegexpException);
		void addZeroOrOne() throw(RegexpException);
		void addOneOrMore() throw(RegexpException);
		void addZeroOrMore() throw(RegexpException);

		Nfa getBuiltNfa() throw(RegexpException);
};

#endif /* NFABUILDER_HPP_ */
