#include "Regexp.hpp"

#include "NfaBuilder.hpp"

#include <iostream>
#include <stack>

Regexp::Regexp() {
	initPriorities();
}

Regexp::~Regexp() { /* empty */ }

void Regexp::initPriorities() {
	priorityMap['('] = 0;
	priorityMap[')'] = 0;
	priorityMap['|'] = 1;
	priorityMap['&'] = 2;
	priorityMap['*'] = 3;
	priorityMap['+'] = 3;
	priorityMap['?'] = 3;
}

void Regexp::setRegexp(const std::string& regexp) throw(RegexpException) {
	infixRegexp = regexp;
	postfixRegexp = evalInToPost(injectAmps());

	buildNfa();
}

bool Regexp::match(const std::string& str) {

	std::string::const_iterator symbol;

	nfa.resetTrace();

	for(symbol = str.begin(); symbol != str.end(); ++symbol) {
		nfa.step(*symbol);
	}

	return nfa.isEnd();
}

void Regexp::buildNfa() {
	NfaBuilder builder;

	std::string::const_iterator symbol;
	for(symbol = postfixRegexp.begin(); symbol != postfixRegexp.end(); ++symbol) {
		switch(*symbol) {
			case '&':
				builder.addConcatenation();
				break;
			case '|':
				builder.addAlternative();
				break;
			case '?':
				builder.addZeroOrOne();
				break;
			case '+':
				builder.addOneOrMore();
				break;
			case '*':
				builder.addZeroOrMore();
				break;
			default:
				builder.addChar(*symbol);
		}
	}

	nfa = builder.getBuiltNfa();
}

std::string Regexp::evalInToPost(const std::string& regexp) {
	std::string result;
	std::stack<char> opStack;

	for(std::string::const_iterator symbol = regexp.begin(); symbol != regexp.end(); ++symbol) {
		if(priorityMap.find(*symbol) == priorityMap.end()) {
			result.push_back(*symbol);
		}
		else if(*symbol == '(') {
			opStack.push(*symbol);
		}
		else if(*symbol == ')') {
			while(!opStack.empty() && opStack.top() != '(') {
				result.push_back(opStack.top());
				opStack.pop();
			}
			opStack.pop();
		}
		else {
			if(opStack.empty() || priorityMap.at(*symbol) > priorityMap.at(opStack.top())) {
				opStack.push(*symbol);
			}
			else {
				while(!opStack.empty() && priorityMap.at(*symbol) <= priorityMap.at(opStack.top())) {
					result.push_back(opStack.top());
					opStack.pop();
				}
				opStack.push(*symbol);
			}
		}
	}
	while(!opStack.empty()) {
		result.push_back(opStack.top());
		opStack.pop();
	}
	return result;
}

std::string Regexp::injectAmps() {
	const std::string& regexp = infixRegexp;
	std::string result;

	std::string::const_iterator symbol, nextSymbol = regexp.begin();
	for(symbol = nextSymbol++; nextSymbol != regexp.end(); ++symbol, ++nextSymbol) {
		char c = *symbol;
		char nc = *nextSymbol;
		result.push_back(c);

		if(c == '&' || c == '|' || c == '(') {
			continue;
		}
		else if(nc != '&' && nc != '|' && nc != ')' && nc != '*' && nc != '+' && nc != '?') {
			result.push_back('&');
		}

	}
	result.push_back(*symbol);
	return result;
}
