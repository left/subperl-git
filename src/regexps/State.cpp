#include "State.hpp"

#include <cstdlib>

State::State() {
	next[0] = NULL;
	next[1] = NULL;
	spliter = false;
	ending = false;
	match = 0;
}

State::State(char c) {
	next[0] = NULL;
	next[1] = NULL;
	spliter = false;
	ending = false;
	match = c;
}

State::~State() {
	delete next[0];
	delete next[1];
}
