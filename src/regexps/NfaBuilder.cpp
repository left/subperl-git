#include "NfaBuilder.hpp"


NfaBuilder::NfaBuilder() { /* empty */ }

NfaBuilder::~NfaBuilder() {
	while(!fragmentStack.empty()) {
		fragmentStack.pop();
	}
}

void NfaBuilder::checkStack(unsigned int size) throw(RegexpException) {
	if(fragmentStack.size() < size) {
		throw RegexpException();
	}
}

Nfa NfaBuilder::getBuiltNfa() throw(RegexpException) {
	checkStack(1);
	Nfa nfa = fragmentStack.top();
	fragmentStack.pop();

	State* end = new State();
	end->setEnding(true);

	nfa.patch(end);

	return nfa;
}

void NfaBuilder::addChar(char c) throw(RegexpException) {
	Nfa nfa;
	State* newState = new State(c);

	nfa.setStart(newState);
	nfa.appendOut(newState->getLeftOutPin());

	fragmentStack.push(nfa);
}

void NfaBuilder::addConcatenation() throw(RegexpException) {
	checkStack(2);
	Nfa nfaSecond = fragmentStack.top();
	fragmentStack.pop();
	Nfa nfaFirst = fragmentStack.top();
	fragmentStack.pop();

	nfaFirst.patch(nfaSecond);
	nfaFirst.appendOut(nfaSecond);
	fragmentStack.push(nfaFirst);
}

void NfaBuilder::addAlternative() throw(RegexpException) {
	checkStack(2);
	Nfa nfaSecond = fragmentStack.top();
	fragmentStack.pop();
	Nfa nfaFirst = fragmentStack.top();
	fragmentStack.pop();

	State* newState = new State();
	newState->setSpliter(true);
	newState->setLeftOut(nfaFirst.getStart());
	newState->setRightOut(nfaSecond.getStart());

	Nfa nfa;
	nfa.setStart(newState);
	nfa.appendOut(nfaFirst);
	nfa.appendOut(nfaSecond);

	fragmentStack.push(nfa);
}

void NfaBuilder::addZeroOrOne() throw(RegexpException) {
	checkStack(1);
	Nfa nfa = fragmentStack.top();
	fragmentStack.pop();

	State* newState = new State();
	newState->setSpliter(true);
	newState->setLeftOut(nfa.getStart());
	Nfa newNfa;
	newNfa.setStart(newState);
	newNfa.appendOut(nfa);
	newNfa.appendOut(newState->getRightOutPin());

	fragmentStack.push(newNfa);
}

void NfaBuilder::addOneOrMore() throw(RegexpException) {
	checkStack(1);
	Nfa nfa = fragmentStack.top();
	fragmentStack.pop();

	State* newState = new State();
	newState->setSpliter(true);
	newState->setLeftOut(nfa.getStart());

	nfa.patch(newState);

	Nfa newNfa;
	newNfa.setStart(nfa.getStart());
	newNfa.appendOut(newState->getRightOutPin());

	fragmentStack.push(newNfa);
}

void NfaBuilder::addZeroOrMore() throw(RegexpException) {
	checkStack(1);
	Nfa nfa = fragmentStack.top();
	fragmentStack.pop();

	State* newState = new State();
	newState->setSpliter(true);
	newState->setLeftOut(nfa.getStart());

	Nfa newNfa;
	newNfa.setStart(newState);

	nfa.patch(newNfa);

//	newNfa.setStart(nfa.getStart());
	newNfa.appendOut(newState->getRightOutPin());

	fragmentStack.push(newNfa);
}
