#include "Nfa.hpp"

#include <iostream>

Nfa::Nfa() {
	currentStates = new StateList();
	nextStates = new StateList();
}

Nfa::~Nfa() {
	delete currentStates;
	delete nextStates;
}

Nfa::Nfa(const Nfa& nfa) {
	start = nfa.start;
	appendOut(nfa);

	currentStates = new StateList();
	nextStates = new StateList();
}

Nfa& Nfa::operator=(const Nfa& nfa) {
	if(this == &nfa) {
		return *this;
	}

	start = nfa.start;
	out.clear();
	appendOut(nfa);

	return *this;
}

void Nfa::appendOut(State** statePin) {
	out.push_back(statePin);
}

void Nfa::appendOut(const Nfa& nfa) {
	out.insert(out.end(), nfa.out.begin(), nfa.out.end());
}

void Nfa::patch(const Nfa& nfa) {
	OutPinsList::iterator it;
	for(it = out.begin(); it != out.end(); ++it) {
		State** pin = *it;
		*pin = nfa.getStart();
	}
	out.clear();
}

void Nfa::patch(State* state) {
	OutPinsList::iterator it;
	for(it = out.begin(); it != out.end(); ++it) {
		State** pin = *it;
		*pin = state;
	}
	out.clear();
}

void Nfa::resetTrace() {
	currentStates->clear();
	nextStates->clear();
	addToNextStates(start);
	std::swap(currentStates, nextStates);
}

void Nfa::step(char c) {
	StateList::iterator matchState;

	for(matchState = currentStates->begin(); matchState != currentStates->end(); ++matchState) {
		if((*matchState)->getMatch() == c) {
			addToNextStates((*matchState)->getLeftOut());
		}
	}

	/* po przejsciu obecnych, nastepne staja sie obecnymi :) */
	currentStates->clear();
	std::swap(currentStates, nextStates);
}

bool Nfa::isEnd() const {
	StateList::iterator endState;

	for(endState = currentStates->begin(); endState != currentStates->end(); ++endState) {
		if((*endState)->isEnding()) {
			return true;
		}
	}

	return false;
}

void Nfa::addToNextStates(State* state) {
	if(state == NULL)
		return;

	if(state->isSpliter()) {
		/* ide za epsilonami */
		addToNextStates(state->getLeftOut());
		addToNextStates(state->getRightOut());
		return;
	}
	nextStates->push_back(state);
}
