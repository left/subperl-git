#ifndef STATE_HPP_
#define STATE_HPP_

/**
 * Klasa reprezentuje stan w NFA (niedeterministycznej maszynie stanowej).
 */
class State {
	private:
		/** Znak ktory dopasowuje stan. */
		char match;
		/** Czy stan jest koncowy. */
		bool ending;
		/** Czy jest rozgalezieniem. */
		bool spliter;

		/** Wyjscia ze stanu. */
		State* next[2];

		/**
		 * Metoda uzywana przez budowniczego do budowania automatow.
		 *
		 * @return Wskazania na pierwsze wyjscie.
		 */
		State** getLeftOutPin() { return &(next[0]); }

		/**
		 * Metoda uzywana przez budowniczego do budowania automatow.
		 *
		 * @return Wskazania na drugie wyjscie.
		 */
		State** getRightOutPin() { return &(next[1]); }

		friend class NfaBuilder;

	public:
		State();
		State(char c);
		~State();

		bool isEnding() const { return ending; }
		bool isSpliter() const { return spliter; }
		char getMatch() const { return match; }

		void setEnding(bool e) { ending = e; }
		void setSpliter(bool s) { spliter = s; }

		State* getLeftOut() const { return next[0]; }
		State* getRightOut() const { return next[1]; }

		void setLeftOut(State* out) { next[0] = out; }
		void setRightOut(State* out) { next[1] = out; }
};

#endif /* STATE_HPP_ */
