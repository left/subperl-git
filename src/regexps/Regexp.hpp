#ifndef REGEXP_HPP_
#define REGEXP_HPP_

#include "Nfa.hpp"
#include "../exceptions/RegexpException.hpp"

#include <string>
#include <boost/unordered_map.hpp>

/**
 * Klasa odpowiedzialna za dopasowywanie wzorcow.
 *
 * Zaimplementowana z wykorzystaniem algorytmu Thompson'a.
 * Zrealizowana przy pomocy artykulu Russ Cox'a.
 * Mozna go znalesc pod adresem: http://swtch.com/~rsc/regexp/regexp1.html
 */
class Regexp {
	private:
		typedef boost::unordered_map<char, int> PriorityMap;

		/** Regexp w zapisie infixowym. */
		std::string infixRegexp;
		/** Regexp w zapisie postfixowym. */
		std::string postfixRegexp;

		/** Automat skonczony dla regexpa. */
		Nfa nfa;

		/** Mapa prioryetetow operator regexpa. */
		PriorityMap priorityMap;

		/**
		 * Inicjalizuje priorytety.
		 */
		void initPriorities();

		/**
		 * Dodaje do regexpu operator konkatenacji w brakujace miejsca.
		 */
		std::string injectAmps();

		/**
		 * Zamienia regexp na postac postfixowa.
		 * @param regexp Regexp do zamiany.
		 */
		std::string evalInToPost(const std::string& regexp);

		/**
		 * Buduje automat z zapisu posftixowego.
		 */
		void buildNfa();

	public:
		Regexp();
		~Regexp();

		/**
		 * Ustawia regexp zmieniajac jego zapis na postfixowy.
		 * @param regexp Regexp do przetworzenia.
		 */
		void setRegexp(const std::string& regexp) throw(RegexpException);

		/**
		 * @return Regexp w postaci postfixowej.
		 */
		std::string getPostfixRegexp() const { return postfixRegexp; };

		/**
		 * @param str String ktory chcemy dopasowac do regexpa.
		 * @return True jesli dopasowanie sie powiodlo, jesli nie to false.
		 */
		bool match(const std::string& str);
};

#endif /* REGEXP_HPP_ */
