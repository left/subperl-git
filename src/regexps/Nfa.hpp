#ifndef NFA_HPP_
#define NFA_HPP_

#include "State.hpp"

#include <list>

/**
 * Klasa reprezentuje niederministyczny automat skonczony.
 * Wykorzystwna do dopasowywyania wzorca.
 */
class Nfa {
	private:
		typedef std::list<State**> OutPinsList;
		typedef std::list<State*> StateList;

		/** Stan startowy automatu. */
		State *start;

		/** Wskazania na wyjscia z automatu. */
		OutPinsList out;

		/** Wskazanie na liste stanow w ktorych znajduje sie automat. */
		StateList* currentStates;
		/** Wskazanie na liste stanow do ktorych moze przejsc automat. */
		StateList* nextStates;

		void addToNextStates(State* state);

	public:
		/** K-tor. */
		Nfa();
		/** D-tor. */
		~Nfa();

		/** K-tor kopiujacy, nie kopiuje wskazan list stanow automatow. */
		Nfa(const Nfa& nfa);
		/** Op przypisania , nie kopiuje wskazan list stanow automatow. */
		Nfa& operator=(const Nfa& nfa);

		/**
		 * @return Stan startowy automatu
		 */
		State* getStart() const { return start; }
		/**
		 * Ustawia stan startowy automatu.
		 */
		void setStart(State* state) { start = state; }

		/**
		 * Dolacza wyjscia z automatu takie same jak ma podany automat.
		 */
		void appendOut(const Nfa& nfa);
		/**
		 * Dolacza jedno konktretne wyjscie z automatu.
		 */
		void appendOut(State** statePin);

		/**
		 * Ustaiwa wyjscia z automatu na start podanego automatu.
		 */
		void patch(const Nfa& nfa);
		/**
		 * Ustaiwa wyjscia z automatu na podany stan.
		 */
		void patch(State* state);

		/**
		 * Czysci listy obecnych i nastepnych stanow automatu.
		 * Dodaje do nich stan startowy.
		 */
		void resetTrace();
		/**
		 * Jeden krok w automacie.
		 * Jesli znak pasuje do stanu to przechodzi do stanow nastepnych.
		 */
		void step(char c);
		/**
		 * @return True jesli automat jest w stanie koncowym.
		 */
		bool isEnd() const;

};

#endif /* NFA_HPP_ */
