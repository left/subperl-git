#include "SymbolTable.hpp"

#include <iostream>

SymbolTable::SymbolTable() { /* empty */ }

SymbolTable::~SymbolTable() { /* empty */ }

void SymbolTable::registerSymbol(const std::string& name, const PSymbol& symbol) {
	if( strToSymbolMap.find(name) != strToSymbolMap.end() ) {
		return;
	}
	strToSymbolMap[name] = symbol;
}

bool SymbolTable::hasName(const std::string& name) const {
	if(strToSymbolMap.find(name) != strToSymbolMap.end()) {
		return true;
	}
	return false;
}

PSymbol SymbolTable::getSymbol(const std::string& id) const {
	StringToSymbolMap::const_iterator it;
	if( ( it = strToSymbolMap.find(id) ) != strToSymbolMap.end() ) {
		return it->second;
	}
	return PSymbol();
}
