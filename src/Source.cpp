#include "Source.hpp"

#include <iostream>
#include <fstream>

Source::Source() : inputStream(&std::cin) {
	incrementLine = false;
}

Source::Source(std::string fileName) : inputStream(NULL), textPosition(fileName) {
	std::ifstream* tmp = new std::ifstream(fileName.c_str(), std::fstream::in);
	if(!tmp->is_open()) {
		inputStream = NULL;
	} else {
		inputStream = tmp;
	}
	incrementLine = false;
}

Source::~Source() {
	if(inputStream != &std::cin && inputStream != NULL) {
		// dynamic_cast<std::ifstream*>(inputStream)->close();
		// delete automaticly closes a file
		delete inputStream;
	}
}

char Source::getNextChar() throw(SourceException) {
	if(!isReady()) {
		throw SourceException(textPosition.getFileName());
	}

	if(incrementLine) {
		incrementLine = false;
		textPosition.incLine();
	}

	char nextChar;
	inputStream->get(nextChar);
	textPosition.incColunm();


	if(!inputStream->good()) {
		nextChar = EOF;
	}
	else if(nextChar == '\n') {
		incrementLine = true;
	}

	return nextChar;
}

TextPosition Source::getTextPosition() const {
	return textPosition;
}

void Source::ungetChar() {
	inputStream->unget();
}

bool Source::isEof() const {
	return inputStream->eof();
}
