#ifndef SCANNER_HPP_
#define SCANNER_HPP_

#include "Source.hpp"
#include "SymbolTable.hpp"
#include "atoms/Atom.hpp"
#include "exceptions/ScannerException.hpp"
#include "symbols/Scalar.hpp"

#include <boost/optional.hpp>
#include <boost/unordered_map.hpp>

/**
 * Klasa reprezentuje analizator leksykalny.
 *
 * Tworzy kolejne atomy leksykalne wykorzystujac klase Source.
 */
class Scanner {
	private:
		/** Typ dla atomu z wartoscia opcjonalna. */
		typedef boost::optional<atoms::Atom> OptionalAtom;
		/** Hash mapa stringow na atomy leksykalne. */
		typedef boost::unordered_map<std::string, atoms::Atom> KeywordMap;
		/** Referencja na obiekt podajacy kolejne znaki. */
		Source& source;
		/** Referencja na tablice symboli. */
		SymbolTable& symbolTable;
		/** Zmienna przechowywuje ostatnio pobrany znak z zrodla. */
		char current;
		/** Mapa slow kluczowych na atomy leksykalne. */
		KeywordMap keywordMap;

		/** Wskazanie na ostatnio skalar. */
		PSymbol lastScalar;
		/** Wskazanie na ostatnia stala. */
		PConstantScalar lastConstantScalar;

		/**
		 * Pobiera kolejny znak i zapisuje w zmiennej current.
		 */
		void getNextChar();

		/**
		 * Zapisuje do mapy slow kluczowych odpowiednie atomy leksykalne.
		 */
		void registerKeywords();

		/**
		 * Ciasteczkowy potwor.
		 *                   _______
		 *    ,,,   . o 0 O (Ale kac)
		 *  <(x_X)>     ._.        ._.
		 * >-|__|-<     | |________| |
		 *   //\\       |_|        |_|
		 *
		 * Na kaca najlepsza praca.
		 *
		 * Sprawdza takze czy nie wstapil koniec pliku.
		 */
		OptionalAtom eatWhiteCharsAndComments();

		/**
		 * Sprawdza czy wystapil identyfikator.
		 */
		OptionalAtom eatIdentifier();

		/**
		 * Sprawdza czy wystapilo slowo kluczowe.
		 */
		OptionalAtom eatKeyword();

		/**
		 * Sprawdza czy wystapila liczba.
		 */
		OptionalAtom eatDigits();

		/**
		 * Sprawdza czy wystapil lancuch znakow.
		 */
		OptionalAtom eatString();

		/**
		 * Sprawdza czy wystapil operator.
		 */
		OptionalAtom eatOperator();

	public:
		/**
		 * Inicjalizuje pola oraz rejestruje slowa kluczowe.
		 * Pobiera pierwszy znak ze zrodla.
		 *
		 * @param source Obiekt inicalizujacy referencje analizatora.
		 * @param symbolTable Obiekt inicjalizujacy referecje tablicy symboli.
		 */
		Scanner(Source& source, SymbolTable& symbolTable);

		/**
		 * D-tor
		 */
		~Scanner();

		/**
		 * Grupuje kolejne znaki w atom leksyklany.
		 *
		 * @return Atom leksyklany.
		 */
		atoms::Atom getNextAtom() throw (ScannerException);

		/**
		 * @return Aktualna pozycja w tekscie.
		 */
		TextPosition getTextPosition() const;

		/**
		 * Przygotowuje scanner do uzycia, pobiera pierwszy znak.
		 */
		void prepare();

		/**
		 * @return Ostatnio wczytana stala.
		 */
		PConstantScalar getLastConstant() const;
		/**
		 * @return Ostatnio wczytana zmienna.
		 */
		PSymbol getLastSymbol() const;
};

#endif /* SCANNER_HPP_ */
