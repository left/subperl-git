#include "Subperl.hpp"

#include <iostream>

Subperl::Subperl() : scanner(source, symbolTable), parser(scanner, calculator) {
	/* empty body */
}

Subperl::Subperl(std::string fileName) : source(fileName), scanner(source, symbolTable), parser(scanner, calculator) {
	/* empty body */
}

Subperl::~Subperl() {
	/* empty body */
}

bool Subperl::isGood() const {
	return source.isReady();
}

int Subperl::execute() {

	try {
		parser.prepare();
		parser.program();
	}
	catch(const SourceException& exception) {
		std::cerr << exception.what() << std::endl;
		return -1;
	}
	catch(const std::exception& exception) {
		std::cerr << source.getTextPosition().toString() << exception.what() << std::endl;
		return -2;
	}

	return 0;
}
