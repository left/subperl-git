#include "Calculator.hpp"

#include "symbols/Scalar.hpp"
#include "regexps/Regexp.hpp"

#include <iostream>
#include <boost/bind.hpp>

Calculator::Calculator() {
	initOperatorPriorityMap();
	initOperatorHandlersMap();
}

Calculator::~Calculator() { /* empty */ }

void Calculator::initOperatorPriorityMap() {
	using namespace atoms;
	atomToPriorityMap[BRACKET_LEFT] = 0;
//	atomToPriorityMap[BRACKET_RIGHT] = 0; // nie musi byc w srodku

	atomToPriorityMap[OR] = 1;
	atomToPriorityMap[AND] = 2;

	atomToPriorityMap[ASSIGNMENT] = 3;
	atomToPriorityMap[PLUS] = 4;
	atomToPriorityMap[MINUS] = 4;

	atomToPriorityMap[MUL] = 5;
	atomToPriorityMap[DIV] = 5;
	atomToPriorityMap[MOD] = 5;

	atomToPriorityMap[EQUAL] = 6;
	atomToPriorityMap[NOT_EQUAL] = 6;

	atomToPriorityMap[LESS] = 7;
	atomToPriorityMap[LESS_EQUAL] = 7;
	atomToPriorityMap[GREATER] = 7;
	atomToPriorityMap[GREATER_EQUAL] = 7;

	atomToPriorityMap[REGEXP_MATCH] = 8;

	//unarne kminy
	atomToPriorityMap[UNARY_PLUS] = 9;
	atomToPriorityMap[UNARY_MINUS] = 9;
	atomToPriorityMap[NOT] = 9;

	atomToPriorityMap[POWER] = 10;
}

void Calculator::initOperatorHandlersMap() {
	using namespace atoms;
	using namespace boost;
//	atomToHandlerMap[BRACKET_LEFT] = ;
//	atomToPriorityMap[BRACKET_RIGHT] = ;
	atomToHandlerMap[ASSIGNMENT] = bind(&Calculator::assignment, this);
	atomToHandlerMap[PLUS] = bind(&Calculator::plus, this);
	atomToHandlerMap[MINUS] = bind(&Calculator::minus, this);
	atomToHandlerMap[MUL] = bind(&Calculator::mul, this);
	atomToHandlerMap[DIV] = bind(&Calculator::div, this);
	atomToHandlerMap[MOD] = bind(&Calculator::mod, this);

	//unarne kminy
	atomToHandlerMap[UNARY_PLUS] = bind(&Calculator::unaryPlus, this);
	atomToHandlerMap[UNARY_MINUS] = bind(&Calculator::unaryMinus, this);

	atomToHandlerMap[POWER] = bind(&Calculator::power, this);
	atomToHandlerMap[LESS] = bind(&Calculator::less, this);
	atomToHandlerMap[LESS_EQUAL] = bind(&Calculator::lessEqual, this);
	atomToHandlerMap[GREATER] = bind(&Calculator::greater, this);
	atomToHandlerMap[GREATER_EQUAL] = bind(&Calculator::greaterEqual, this);
	atomToHandlerMap[EQUAL] = bind(&Calculator::equal, this);
	atomToHandlerMap[NOT_EQUAL] = bind(&Calculator::notEqual, this);
	atomToHandlerMap[REGEXP_MATCH] = bind(&Calculator::regexpMatch, this);
	atomToHandlerMap[OR] = bind(&Calculator::orRelation, this);
	atomToHandlerMap[AND] = bind(&Calculator::andRelation, this);
	atomToHandlerMap[NOT] = bind(&Calculator::notRelation, this);
}

void Calculator::appendSymbol(PSymbol symbol) {
	outputStack.push_back(symbol);
}

void Calculator::appendOperator(POperatorSymbol symbol) {
	if(symbol->getOperatorType() == atoms::BRACKET_LEFT) {
		operatorStack.push_back(symbol);
	}
	else if(symbol->getOperatorType() == atoms::BRACKET_RIGHT) {
		while(!operatorStack.empty() && operatorStack.back()->getOperatorType() != atoms::BRACKET_LEFT) {
			outputStack.push_back(operatorStack.back());
			operatorStack.pop_back();
		}
		operatorStack.pop_back();
	}
	else {
		if(operatorStack.empty() || atomToPriorityMap.at(symbol->getOperatorType()) > atomToPriorityMap.at(operatorStack.back()->getOperatorType())
				|| (operatorStack.back()->isUnary() && symbol->isUnary())) {
			operatorStack.push_back(symbol);
		}
		else {
			while(!operatorStack.empty() && atomToPriorityMap.at(symbol->getOperatorType()) <= atomToPriorityMap.at(operatorStack.back()->getOperatorType())) {
				outputStack.push_back(operatorStack.back());
				operatorStack.pop_back();
			}
			operatorStack.push_back(symbol);
		}
	}

}

// niezmiennik - po wywolaniu calculate struktury danych sa puste
PConstantScalar Calculator::calculate() throw(DivisionByZero, RegexpException) {
	while(!operatorStack.empty()) {
		outputStack.push_back(operatorStack.back());
		operatorStack.pop_back();
	}
	while(!outputStack.empty()) {
		PSymbol currentSymbol = outputStack.front();
		outputStack.pop_front();

		if(currentSymbol->getType() == Symbol::SCALAR || currentSymbol->getType() == Symbol::CONSTANT) {
			calculatorStack.push_back(currentSymbol);
		}
		else if(currentSymbol->getType() == Symbol::OPERATOR) {
			handleOperator(boost::dynamic_pointer_cast<OperatorSymbol>(currentSymbol));
		}
	}
	PSymbol retValue = calculatorStack.back();
	calculatorStack.pop_back();
	return boost::dynamic_pointer_cast<ConstantScalar>(retValue);
}

void Calculator::handleOperator(POperatorSymbol symbol) {
	OperatorHandlerFunctor handlerFunction = atomToHandlerMap.at(symbol->getOperatorType());
	handlerFunction();
}

void Calculator::assignment() {
	PConstantScalar op1 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();
	PScalar op2 = boost::dynamic_pointer_cast<Scalar>(calculatorStack.back());

	op2->setValue(*(op1.get()));
}

void Calculator::plus() {
	PConstantScalar op1 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();
	PConstantScalar op2 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();

	PConstantScalar result;

	if(op1->isInteger() && op2->isInteger()) {
		result = PConstantScalar(new ConstantScalar(op1->getIntValue() + op2->getIntValue()));
	}
	else {
		result = PConstantScalar(new ConstantScalar(op1->getFloatValue() + op2->getFloatValue()));
	}
	calculatorStack.push_back(result);
}

void Calculator::minus() {
	PConstantScalar op1 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();
	PConstantScalar op2 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();

	PConstantScalar result;

	if(op1->isInteger() && op2->isInteger()) {
		result = PConstantScalar(new ConstantScalar(op2->getIntValue() - op1->getIntValue()));
	}
	else {
		result = PConstantScalar(new ConstantScalar(op2->getFloatValue() - op1->getFloatValue()));
	}
	calculatorStack.push_back(result);
}

void Calculator::mul() {
	PConstantScalar op1 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();
	PConstantScalar op2 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();

	PConstantScalar result;

	if(op1->isInteger() && op2->isInteger()) {
		result = PConstantScalar(new ConstantScalar(op2->getIntValue() * op1->getIntValue()));
	}
	else {
		result = PConstantScalar(new ConstantScalar(op2->getFloatValue() * op1->getFloatValue()));
	}
	calculatorStack.push_back(result);
}

void Calculator::div() throw(DivisionByZero) {
	PConstantScalar op1 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();
	PConstantScalar op2 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();

	if(op1->getFloatValue() == 0.0f) {
		throw DivisionByZero();
	}

	PConstantScalar result = PConstantScalar(new ConstantScalar(op2->getFloatValue() / op1->getFloatValue()));
	calculatorStack.push_back(result);
}

void Calculator::mod() {
	PConstantScalar op1 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();
	PConstantScalar op2 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();

	if(op1->getIntValue() == 0.0f) {
		throw DivisionByZero();
	}

	PConstantScalar result = PConstantScalar(new ConstantScalar(op2->getIntValue() % op1->getIntValue()));
	calculatorStack.push_back(result);
}

void Calculator::unaryPlus() {
	// nothing to do :)
}

void Calculator::unaryMinus() {
	PConstantScalar op1 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();

	PConstantScalar result;

	if(op1->isInteger()) {
		result = PConstantScalar(new ConstantScalar( - op1->getIntValue() ));
	}
	else {
		result = PConstantScalar(new ConstantScalar( - op1->getFloatValue() ));
	}
	calculatorStack.push_back(result);
}

void Calculator::power() {
	PConstantScalar op1 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();
	PConstantScalar op2 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();

	PConstantScalar result;

	if(op1->isInteger() && op2->isInteger() && op1->getIntValue() >= 0) {
		result = PConstantScalar(new ConstantScalar( static_cast<int>(powf(op2->getFloatValue(), op1->getFloatValue())) ));
	}
	else {
		result = PConstantScalar(new ConstantScalar( powf(op2->getFloatValue(), op1->getFloatValue()) ));
	}
	calculatorStack.push_back(result);
}

void Calculator::less() {
	PConstantScalar op1 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();
	PConstantScalar op2 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();

	PConstantScalar result;
	if(op2->getFloatValue() < op1->getFloatValue()) {
		result = PConstantScalar(new ConstantScalar( 1 ));
	}
	else {
		result = PConstantScalar(new ConstantScalar( 0 ));
	}
	calculatorStack.push_back(result);
}

void Calculator::lessEqual() {
	PConstantScalar op1 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();
	PConstantScalar op2 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();

	PConstantScalar result;
	if(op2->getFloatValue() <= op1->getFloatValue()) {
		result = PConstantScalar(new ConstantScalar( 1 ));
	}
	else {
		result = PConstantScalar(new ConstantScalar( 0 ));
	}
	calculatorStack.push_back(result);
}

void Calculator::greater() {
	PConstantScalar op1 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();
	PConstantScalar op2 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();

	PConstantScalar result;
	if(op2->getFloatValue() > op1->getFloatValue()) {
		result = PConstantScalar(new ConstantScalar( 1 ));
	}
	else {
		result = PConstantScalar(new ConstantScalar( 0 ));
	}
	calculatorStack.push_back(result);
}

void Calculator::greaterEqual() {
	PConstantScalar op1 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();
	PConstantScalar op2 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();

	PConstantScalar result;
	if(op2->getFloatValue() >= op1->getFloatValue()) {
		result = PConstantScalar(new ConstantScalar( 1 ));
	}
	else {
		result = PConstantScalar(new ConstantScalar( 0 ));
	}
	calculatorStack.push_back(result);
}

void Calculator::equal() {
	PConstantScalar op1 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();
	PConstantScalar op2 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();

	PConstantScalar result;
	if(op2->getFloatValue() == op1->getFloatValue()) {
		result = PConstantScalar(new ConstantScalar( 1 ));
	}
	else {
		result = PConstantScalar(new ConstantScalar( 0 ));
	}
	calculatorStack.push_back(result);
}

void Calculator::notEqual() {
	PConstantScalar op1 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();
	PConstantScalar op2 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();

	PConstantScalar result;
	if(op2->getFloatValue() != op1->getFloatValue()) {
		result = PConstantScalar(new ConstantScalar( 1 ));
	}
	else {
		result = PConstantScalar(new ConstantScalar( 0 ));
	}
	calculatorStack.push_back(result);
}

void Calculator::regexpMatch() {
	PConstantScalar op1 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();
	PConstantScalar op2 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();

	PConstantScalar result;
	Regexp regexp;

	regexp.setRegexp(op1->getStringValue());

	if(regexp.match(op2->getStringValue())) {
		result = PConstantScalar(new ConstantScalar( op2->getStringValue() ));
	}
	else {
		result = PConstantScalar(new ConstantScalar( "" ));
	}
	calculatorStack.push_back(result);
}

void Calculator::orRelation() {
	PConstantScalar op1 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();
	PConstantScalar op2 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();

	PConstantScalar result;
	if(op2->isTrue()) {
		result = PConstantScalar(new ConstantScalar( *op2 ));
	}
	else if(op1->isTrue()) {
		result = PConstantScalar(new ConstantScalar( *op1 ));
	}
	else {
		result = PConstantScalar(new ConstantScalar( 0 ));
	}
	calculatorStack.push_back(result);
}

void Calculator::andRelation() {
	PConstantScalar op1 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();
	PConstantScalar op2 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();

	PConstantScalar result;
	if(!op2->isTrue()) {
		result = PConstantScalar(new ConstantScalar( 0 ));
	}
	else if(!op1->isTrue()) {
		result = PConstantScalar(new ConstantScalar( 0 ));
	}
	else {
		result = PConstantScalar(new ConstantScalar( *op1 ));
	}
	calculatorStack.push_back(result);
}

void Calculator::notRelation() {
	PConstantScalar op1 = boost::dynamic_pointer_cast<ConstantScalar>(calculatorStack.back());
	calculatorStack.pop_back();

	PConstantScalar result = PConstantScalar(new ConstantScalar( !op1->getFloatValue() ));
	calculatorStack.push_back(result);
}

