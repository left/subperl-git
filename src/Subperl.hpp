#ifndef SUBPERL_HPP_
#define SUBPERL_HPP_

#include "Source.hpp"
#include "SymbolTable.hpp"
#include "Scanner.hpp"
#include "Calculator.hpp"
#include "Parser.hpp"

/**
 * Klasa bedaca fasada na caly interpreter perla.
 */
class Subperl {
	private:
		/** Obiekt odpowiedzialny za obsluge zrodla skryptu. */
		Source source;
		/** Obiekt w ktorym rejestrowane sa zmienne. */
		SymbolTable symbolTable;
		/** Obiekt na podstawie zrodla zwraca leksemy dla parsera.  */
		Scanner scanner;

		/** Obiekt obliczajacy wyrazenia perlowe otrzymane od parsera. */
		Calculator calculator;
		/** Sprawdza poprawnosc skladniowa kodu i dostarcza wyrazenia do calculatora. */
		Parser parser;


	public:
		/**
		 * Domyslny konstruktor.
		 * Wczytuje wejscie z stdin (domslne w klasie Source).
		 */
		Subperl();

		/**
		 * Ustawia wejscie interpretera na podany plik.
		 *
		 * @param fileName Nazwa pliku wejsciowego.
		 */
		Subperl(std::string fileName);

		/** Domyslny destruktor. */
		~Subperl();

		/**
		 * Wykonuje program wypisujac przechwycone bledy jesli wysatpily na stderr.
		 *
		 * @return Zwraca 0 jesli wykonanie powiodlo sie, jesli nie to 1.
		 */
		int execute();

		/**
		 * @return True jesli subperl jest gotowy do wykonania skryptu.
		 */
		bool isGood() const;
};

#endif /* SUBPERL_HPP_ */
