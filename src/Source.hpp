#ifndef SOURCE_HPP_
#define SOURCE_HPP_

#include "utils/TextPosition.hpp"
#include "exceptions/SourceException.hpp"


#include <string>
#include <istream>

/**
 * Klasa opakowuje plik wejsciowy.
 *
 * Odpowiedzialna za podstawowe operacje na pliku takie jak odczyt znakow.
 */
class Source {
	private:
		/** Strumien wejsciowy. */
		std::istream* inputStream;
		/** Aktualna pozycja w tekscie. */
		TextPosition textPosition;
		/** Zmienna pamieta czy ostatnio wystopila nowa linia. */
		bool incrementLine; // konieczne aby pokazywac dobrze aktualne miejce w pliku

	public:
		/**
		 * Domyslny konstruktor ustawia strumien wejsciowy na standardowe wejscie (std::cin).
		 */
		Source();

		/**
		 * Konstruktor otwiera plik do odczytu na podstawie podanej nazwy.
		 *
		 * @param fileName Nazwa pliku wejsciowego
		 */
		Source(std::string fileName);

		/** Destruktor zamyka plik wejsciowy. */
		~Source();

		/**
		 * Przesuwa pozycje w pliku odczytujac i zwracajac kolejny znak.
		 *
		 * @return Kolejny znak z pliku.
		 */
		char getNextChar() throw(SourceException);

		/**
		 * @return Aktualna pozycja w tekscie.
		 */
		TextPosition getTextPosition() const;

		/**
		 * Przesuwa pozycje w pliku o jeden znak do tylu.
		 */
		void ungetChar();

		/**
		 * @return True jesli nastapil koniec pliku.
		 */
		bool isEof() const;

		/**
		 * Sprawdza czy mozna czytac ze zrodla.
		 */
		bool isReady() const { return inputStream!=NULL; }
};

#endif /* SOURCE_HPP_ */
