#ifndef ATOMSSET_HPP_
#define ATOMSSET_HPP_

#include "Atom.hpp"
#include <boost/unordered_set.hpp>

/**
 * Klasa reprezentujaca zbior atomow.
 * Uzywana np do tworzenia zbiorow first.
 */
class AtomsSet {
	private:
		typedef boost::unordered_set<int> InterialAtomsSet;
		/** Wewnetrzny zbior atomow. */
		InterialAtomsSet atomsSet;

	public:
		/**
		 * K-tor domyslny ustawia zbior pusty.
		 */
		AtomsSet();
		/**
		 * Zbior jednoelementowy.
		 *
		 * @param atom Element zbioru.
		 */
		AtomsSet(int atom);
		/**
		 * K-tor o dowolnej liczbie parametrow.
		 * Wklada parametry do zbioru, ostatni paramentr powinien byc atoms::END.
		 */
		AtomsSet(int, int, ...);
		/** D-tor. */
		~AtomsSet();

		/**
		 * Suma zbiorow.
		 *
		 * @param Zbior do zsumowania.
		 * @return Zbior zawierajacy sume elementow obydwu zbiorow.
		 */
		AtomsSet operator+(const AtomsSet&) const;
		/**
		 * Sprawdza czy zbior zawiera dany element.
		 *
		 * @param atom Atom do sprawdzenia.
		 * @return True jesli podany atom jest w zbiorze.
		 */
		bool has(const int& atom) const;

		/**
		 * Do wypisywania zbioru na strumien.
		 */
		friend std::ostream& operator<<(std::ostream& os, const AtomsSet& set);
};

#endif /* ATOMSSET_HPP_ */
