#ifndef ATOM_HPP_
#define ATOM_HPP_

#include <string>

// przestrzen nazw ze wzgledu na brzydkie enumy w cpp
namespace atoms {
	/**
	 * Enumeracja okreslajaca typ atomu leksyklanego.
	 */
	enum Atom {
		ILLEGAL,
		EOT, //!< Koniec pliku
		IDENTIFIER, //!< Identyfikator zmiennej
		INTEGER, //!< Stala calkowita
		FLOAT, //!< Stala zmienno przecinkowa
		QUOTED, // albo jeden STRING
		DOUBLE_QUOTED,
		POWER, //!< Operator potegowania
		MUL, //!< Operator mnozenia
		PLUS, //!< Operator dodawania
		UNARY_PLUS, //!< Operator unarny (dodatnia wartosc)
		MINUS, //!< Operator odejmowania
		UNARY_MINUS, //!< Operator dodawania (ujemna wartosc)
		DIV, //!< Operator dzielenia
		MOD, //!< Operator modulo
		CONCAT, //!< Operator konkatenacji lancuchow
		BRACKET_LEFT, //!< Lewy nawias okragly
		BRACKET_RIGHT, //!< Prawy nawias okragly
		SEMICOLON, //!< Srednik oddzielajacy instrukcje w bloku
		SCOPE_BEGIN, //!< Poczatek bloku instrukcji
		SCOPE_END, //!< Koniec bloku instrukcji
		ASSIGNMENT, //!< Operator przypisania
		REGEXP_MATCH, //!< Operator dopasowania wzorca
		GREATER, //!< Operator porownania ">"
		GREATER_EQUAL, //!< Operator porownania ">="
		LESS, //!< Operator porownania "<"
		LESS_EQUAL, //!< Operator porownania "<="
		EQUAL, //!< Operator porownania "=="
		NOT_EQUAL, //!< Operator porownania "!="
		NOT, //!< Operator negacji
		AND, //!< Operator koniunkcji
		OR, //!< Operator alternatywy
		IF, //!< Slowo kluczowe "if"
		ELSIF, //!< Slowo kluczowe "elsif"
		ELSE, //!< Slowo kluczowe "else"
		PRINT, //!< Slowo kluczowe "print"

		END //!< Ostatni atom
	};

	/**
	 * Zamienia wartosc enuma na sensowny string.
	 */
	std::string atomToString(const Atom& atom);
}

#endif /* ATOM_HPP_ */
