#include "AtomsSet.hpp"

#include <cstdarg>

AtomsSet::AtomsSet() { /* empty body */ }

AtomsSet::AtomsSet(int atom) {
	atomsSet.insert(atom);
}

AtomsSet::AtomsSet(int a1, int a2, ...) {
	atomsSet.insert(a1);

	va_list vlist;
	int atom = a2;
	va_start(vlist, a2);
	while(atom != atoms::END) {
		atomsSet.insert(atom);
		atom = va_arg(vlist, int);
	}
	va_end(vlist);
}

AtomsSet::~AtomsSet() { /* empty body */ }

AtomsSet AtomsSet::operator+(const AtomsSet& set) const {
	AtomsSet retSet = *this;
	retSet.atomsSet.insert(set.atomsSet.begin(), set.atomsSet.end());
	return retSet;
}

bool AtomsSet::has(const int& atom) const {
	return atomsSet.find(atom) != atomsSet.end();
}

std::ostream& operator<<(std::ostream& os, const AtomsSet& set) {
	AtomsSet::InterialAtomsSet::const_iterator it;
	for(it = set.atomsSet.begin(); it != set.atomsSet.end(); ++it) {
		atoms::Atom a = (atoms::Atom)(*it % atoms::END);
		os << "'" << atoms::atomToString(a) << "', ";
	}
	//os << "\n";
	return os;
}
