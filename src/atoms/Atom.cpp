#include "Atom.hpp"

#include <map>

namespace atoms {
	std::string atomToString(const Atom& atom) {
		std::map<Atom, std::string> ats;
		ats[ILLEGAL]="ILLEGAL";
		ats[EOT]="EOF";
		ats[IDENTIFIER]="IDENTIFIER";
		ats[INTEGER]="INTEGER";
		ats[FLOAT]="FLOAT";
		ats[QUOTED]="'";
		ats[DOUBLE_QUOTED]="\"";
		ats[POWER]="**";
		ats[MUL]="*";
		ats[PLUS]="+";
		ats[UNARY_PLUS]="+";
		ats[MINUS]="-";
		ats[UNARY_MINUS]="+";
		ats[DIV]="/";
		ats[MOD]="%";
		ats[CONCAT]=".";
		ats[BRACKET_LEFT]="(";
		ats[BRACKET_RIGHT]=")";
		ats[SEMICOLON]=";";
		ats[SCOPE_BEGIN]="{";
		ats[SCOPE_END]="}";
		ats[ASSIGNMENT]="=";
		ats[REGEXP_MATCH]="=~";
		ats[GREATER]=">";
		ats[GREATER_EQUAL]=">=";
		ats[LESS]="<";
		ats[LESS_EQUAL]="<=";
		ats[EQUAL]="==";
		ats[NOT_EQUAL]="!=";
		ats[NOT]="!";
		ats[AND]="and";
		ats[OR]="or";
		ats[IF]="if";
		ats[ELSIF]="elsif";
		ats[ELSE]="else";
		ats[PRINT]="print";
		ats[END]="END";

		return ats[atom];
	}
}
