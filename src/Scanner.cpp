#include "Scanner.hpp"
#include "Source.hpp"
#include "symbols/Scalar.hpp"

#include <boost/lexical_cast.hpp>

#include <iostream>

Scanner::Scanner(Source& _source, SymbolTable& st) : source(_source), symbolTable(st) {
	registerKeywords();
}

Scanner::~Scanner() { /* empty */ }

void Scanner::getNextChar() {
	current = source.getNextChar();
}

void Scanner::prepare() {
	getNextChar();
}

void Scanner::registerKeywords() {
	keywordMap["if"] = atoms::IF;
	keywordMap["elsif"] = atoms::ELSIF;
	keywordMap["else"] = atoms::ELSE;
	keywordMap["not"] = atoms::NOT;
	keywordMap["and"] = atoms::AND;
	keywordMap["or"] = atoms::OR;
	// to moglaby byc predefiniowana funkcja a nie slowo kluczowe
	// jednak nie bedzie implementacji funkcji dlatego print jest jako slowo kluczowe
	keywordMap["print"] = atoms::PRINT;
}

TextPosition Scanner::getTextPosition() const {
	return source.getTextPosition();
}

Scanner::OptionalAtom Scanner::eatWhiteCharsAndComments() {
	do {
		if(current == EOF)
			return OptionalAtom(atoms::EOT);
		if(current == '#') {
			do {
				getNextChar();
			} while(current != '\n');
		}
		while(isspace(current))
			getNextChar();	// Pomin znaki biale
	} while(isspace(current) || current == '#' || current == EOF);
	return OptionalAtom();
}

Scanner::OptionalAtom Scanner::eatIdentifier() {
	std::string scalarName;

	// $ - identyfikator typu skalara
	if(current == '$') {
		getNextChar();
		if(!isalpha(current) && current != '_') {
			throw ScannerException(source.getTextPosition(), current);
//			return OptionalAtom(atoms::ILLEGAL);
		}
		do {
			scalarName.push_back(current);
			getNextChar();
		} while(isalnum(current) || current == '_');

		if(symbolTable.hasName(scalarName)) {
			lastScalar = symbolTable.getSymbol(scalarName);
		}
		else {
			lastScalar = PScalar(new Scalar());
			symbolTable.registerSymbol(scalarName, lastScalar);
		}

		return OptionalAtom(atoms::IDENTIFIER);
	}
	return OptionalAtom();
}

Scanner::OptionalAtom Scanner::eatKeyword() {
	if(!isalpha(current))
		return OptionalAtom();

	std::string keyword;
	KeywordMap::const_iterator it;

	do {
		keyword.push_back(current);
		getNextChar();
	} while(isalpha(current));
	if((it=keywordMap.find(keyword)) != keywordMap.end())
		return OptionalAtom(it->second);
	else {
		//TODO blad mowiacy ze wywolania funkcji nie wspierane
		throw ScannerException(source.getTextPosition(), keyword);
//		return OptionalAtom(atoms::ILLEGAL);
	}
}

Scanner::OptionalAtom Scanner::eatDigits() {
	if(!isdigit(current) && current != '.')
		return OptionalAtom();

	std::string stringValue;

	while(isdigit(current)) {
		// zapisywac wartosc
		stringValue.push_back(current);
		getNextChar();
	}

	if(isalpha(current) || current == '$') { // blad
		throw ScannerException(source.getTextPosition(), current);
//		return OptionalAtom(atoms::ILLEGAL);
	}
	else if(current == '.') {
		do {
			stringValue.push_back(current);
			getNextChar();
		} while(isdigit(current));

		if(isalpha(current)  || current == '$') { // blad
			throw ScannerException(source.getTextPosition(), current);
//			return OptionalAtom(atoms::ILLEGAL);
		}

		lastConstantScalar = PConstantScalar(new ConstantScalar(boost::lexical_cast<float>(stringValue)));
		return OptionalAtom(atoms::FLOAT);
	}

	lastConstantScalar = PConstantScalar(new ConstantScalar(boost::lexical_cast<int>(stringValue)));
	return OptionalAtom(atoms::INTEGER);
}

// mozna dodac interpolacje np dla "ania ma $kota"
Scanner::OptionalAtom Scanner::eatString() {
	std::string stringValue;

	if(current == '\'') {
		getNextChar();
		while(current != '\'') {
			if(current == '\\') {
				getNextChar();
				if(current == '\'') {
					stringValue.push_back('\'');
					getNextChar();
				}
				else {
					stringValue.push_back('\\');
				}
			}
			else {
				stringValue.push_back(current);
				getNextChar();
			}
		}
		getNextChar(); // szykujemy nastepny znak
		lastConstantScalar = PConstantScalar(new ConstantScalar(stringValue));
		return OptionalAtom(atoms::QUOTED);
	}
	else if(current == '"') { // osobny if ze wzgledu na interpolacje
		getNextChar();
		while(current != '"') {
			if(current == '\\') {
				getNextChar();
				if(current == '\"') {
					stringValue.push_back('\"');
					getNextChar();
				}
				else if(current == 'n') {
					stringValue.push_back('\n');
					getNextChar();
				}
				else if(current == 't') {
					stringValue.push_back('\t');
					getNextChar();
				}
				else {
					stringValue.push_back('\\');
				}
			}
			else {
				stringValue.push_back(current);
				getNextChar();
			}
		}
		getNextChar(); // szykujemy nastepny znak
		lastConstantScalar = PConstantScalar(new ConstantScalar(stringValue));
		return OptionalAtom(atoms::DOUBLE_QUOTED);
	}
	return OptionalAtom();
}

// TODO && || itp
Scanner::OptionalAtom Scanner::eatOperator() {
	switch(current) {
		case '*':
			getNextChar();
			if(current != '*')
				return OptionalAtom(atoms::MUL);
			getNextChar(); // szykujemy nastepny znak
			return OptionalAtom(atoms::POWER);
		case '>':
			getNextChar();
			if(current != '=')
				return OptionalAtom(atoms::GREATER);
			getNextChar(); // szykujemy nastepny znak
			return OptionalAtom(atoms::GREATER_EQUAL);
		case '<':
			getNextChar();
			if(current != '=')
				return OptionalAtom(atoms::LESS);
			getNextChar(); // szykujemy nastepny znak
			return OptionalAtom(atoms::LESS_EQUAL);
		case '=':
			getNextChar();
			if(current == '=') {
				getNextChar(); // szykujemy nastepny znak
				return OptionalAtom(atoms::EQUAL);
			} else if(current == '~') {
				getNextChar(); // szykujemy nastepny znak
				return OptionalAtom(atoms::REGEXP_MATCH);
			}
			return OptionalAtom(atoms::ASSIGNMENT);
		case '!':
			getNextChar();
			if(current != '=')
				return OptionalAtom(atoms::NOT);
			getNextChar(); // szykujemy nastepny znak
			return OptionalAtom(atoms::NOT_EQUAL);
		case '+': getNextChar(); return OptionalAtom(atoms::PLUS);
		case '-': getNextChar(); return OptionalAtom(atoms::MINUS);
		case '/': getNextChar(); return OptionalAtom(atoms::DIV);
		case '%': getNextChar(); return OptionalAtom(atoms::MOD);
		case '(': getNextChar(); return OptionalAtom(atoms::BRACKET_LEFT);
		case ')': getNextChar(); return OptionalAtom(atoms::BRACKET_RIGHT);
		case '.': getNextChar(); return OptionalAtom(atoms::CONCAT);
		case ';': getNextChar(); return OptionalAtom(atoms::SEMICOLON);
		case '{': getNextChar(); return OptionalAtom(atoms::SCOPE_BEGIN);
		case '}': getNextChar(); return OptionalAtom(atoms::SCOPE_END);
	}
	return OptionalAtom();
}

atoms::Atom Scanner::getNextAtom() throw (ScannerException) {
	OptionalAtom optionalAtom;

	if(optionalAtom = eatWhiteCharsAndComments()) //
		return optionalAtom.get();
	if(optionalAtom = eatIdentifier()) // sprawdza czy mamy identyfikator zmiennej
		return optionalAtom.get();
	if(optionalAtom = eatKeyword()) // sprawdza czy mamy slowo kluczowe
		return optionalAtom.get();
	if(optionalAtom = eatDigits()) // sprawdza czy mamy liczbe
		return optionalAtom.get();
	if(optionalAtom = eatString()) // sprawdza czy mamy stringi :D
		return optionalAtom.get();
	if(optionalAtom = eatOperator()) // sprawdza czy mamy operator
		return optionalAtom.get();

//	return atoms::ILLEGAL;
	throw ScannerException(source.getTextPosition(), current);
}

PConstantScalar Scanner::getLastConstant() const {
	return lastConstantScalar;
}

PSymbol Scanner::getLastSymbol() const {
	return lastScalar;
}
