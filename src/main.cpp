#include <iostream>

#include "Source.hpp"
#include "Scanner.hpp"
#include "Parser.hpp"
#include "utils/Trace.hpp"

#include "regexps/Regexp.hpp"
#include "Subperl.hpp"

#include <boost/program_options.hpp>

int main(int argc, char *argv[]) {

	namespace po = boost::program_options;
	po::options_description desc("Program is the interpreter of Perl subset.\n"
									"Comes into existance due to technique of compilation course.\n\n"
									"Avaliable options");

	// Deklaracja wspieranych opcji programu
	desc.add_options()
				("help,h", "produce help message")
				("file,f", po::value<std::string>(), "input file name of perl script")
				("stdin,i", "reads perl script from stdin")
				;

    po::positional_options_description p;
    p.add("file", 1);
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
	po::notify(vm);

	std::string fileName;
	int returnValue = 0;

	if(vm.count("file")) {
		fileName = vm["file"].as<std::string>();
		returnValue = Subperl(fileName).execute();
	}
	else if(vm.count("stdin")) {
		returnValue = Subperl().execute();
	}
	else {
		std::cout << desc;
		return 1;
	}

	return returnValue;
}
