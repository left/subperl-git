#ifndef SCANNEREXCEPTION_HPP_
#define SCANNEREXCEPTION_HPP_

#include "../utils/TextPosition.hpp"

#include <exception>

/**
 * Klasa reprezentuje wyjatek zwiazany z bledem skanowania.
 */
class ScannerException : public std::exception {
	private:
		/** Pozycja w tekscie gdzie wystapil blad. */
		TextPosition textPosition;
		/** Znak na ktorym wykryto blad. */
		std::string currentBadChar;

	public:
		ScannerException(const TextPosition& textPos, const char& c)
			: textPosition(textPos), currentBadChar() {
			currentBadChar.push_back(c);
		}
		ScannerException(const TextPosition& textPos, const std::string& str)
					: textPosition(textPos), currentBadChar(str) {}

		~ScannerException() throw() {}

		// inherited
		const char* what() const throw() {
			std::string msg;// = textPosition.toString();
			msg += " Scanner error: unexpected ";
			msg += currentBadChar;
			return msg.c_str();
		}

};

#endif /* SCANNEREXCEPTION_HPP_ */
