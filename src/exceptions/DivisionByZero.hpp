#ifndef DIVISIONBYZERO_HPP_
#define DIVISIONBYZERO_HPP_

#include <exception>

/**
 * Wyjatek rzucany gdy wystepuje dzielenie przez zero.
 */
class DivisionByZero : public std::exception {
	public:
//		DivisionByZero() {}
		~DivisionByZero() throw() {}

		// inherited
		const char* what() const throw() {
			return " Calculator: Illegal division by zero";
		}
};

#endif /* DIVISIONBYZERO_HPP_ */
