#ifndef SOURCEEXCEPTION_HPP_
#define SOURCEEXCEPTION_HPP_

#include <string>
#include <exception>

/**
 * Wyjatek rzucany przez obiekty klasy Source.
 */
class SourceException : public std::exception {
	private:
		std::string fileName;
	public:
		SourceException(std::string file) : fileName(file) {}
		~SourceException() throw() {}

		// inherited
		const char* what() const throw() {
			std::string msg;
			msg += "Source error: cannot open file: ";
			msg += fileName;
			return msg.c_str();
		}
};

#endif /* SOURCEEXCEPTION_HPP_ */
