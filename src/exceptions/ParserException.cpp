#include "ParserException.hpp"

#include <sstream>

ParserException::ParserException(const TextPosition& textPos, const atoms::Atom& atom, const AtomsSet& expected)
		: textPosition(textPos), currentAtom(atom), expectedAtoms(expected) {
	/* empty body */
}

ParserException::~ParserException() throw() { /* empty */ }

const char* ParserException::what() const throw() {
	std::stringstream msg;
	msg 	/*<< textPosition.toString()*/
			<< " Parser error: unexpected atom '"
			<< atoms::atomToString(currentAtom)
			<< "', expected "
			<< expectedAtoms;
	return msg.str().c_str();
}
