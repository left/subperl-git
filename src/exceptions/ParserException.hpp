#ifndef PARSEREXCEPTION_HPP_
#define PARSEREXCEPTION_HPP_

#include <exception>
#include "../atoms/Atom.hpp"
#include "../atoms/AtomsSet.hpp"
#include "../utils/TextPosition.hpp"

/**
 * Klasa reprezentuje wyjatek zwiazany z bledem analizy skladniowej.
 */
class ParserException : public std::exception {
	private:
		/** Pozycja w tekscie gdzie wystapil blad. */
		TextPosition textPosition;
		/** Atom na ktorym wykryto blad. */
		atoms::Atom currentAtom;
		/** Atomy na ktore oczekiwano. */
		AtomsSet expectedAtoms;

	public:
		ParserException(const TextPosition& textPos, const atoms::Atom& atom, const AtomsSet& expected);
		~ParserException() throw();

		// inherited
		const char* what() const throw();
};

#endif /* PARSEREXCEPTION_HPP_ */
