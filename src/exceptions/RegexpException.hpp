#ifndef REGEXPEXCEPTION_HPP_
#define REGEXPEXCEPTION_HPP_

#include <exception>

/**
 * Wyjatek rzucany gdy regexp jest bledny.
 */
class RegexpException : public std::exception {
	public:
		RegexpException() {}
		~RegexpException() throw() {}

		// inherited
		const char* what() const throw() {
			return " Calculator: Cannot parse regexp";
		}
};

#endif /* REGEXPEXCEPTION_HPP_ */
