#ifndef REGEXPTEST_HPP_
#define REGEXPTEST_HPP_

#include "../regexps/Regexp.hpp"

/**
 * Klasa testujaca regexpy.
 */
class RegexpTest {
	public:
		void testPostfix();
		void testMatchRegexp();
};

#endif /* REGEXPTEST_HPP_ */
