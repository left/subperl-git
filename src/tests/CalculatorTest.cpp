#include "CalculatorTest.hpp"

#include <boost/test/unit_test.hpp>

void CalculatorTest::calculateTestOne() {
	Calculator c;

	c.appendOperator(POperatorSymbol(new OperatorSymbol(atoms::BRACKET_LEFT)));
	c.appendSymbol(PConstantScalar(new ConstantScalar(3)));
	c.appendOperator(POperatorSymbol(new OperatorSymbol(atoms::PLUS)));
	c.appendSymbol(PConstantScalar(new ConstantScalar(4)));
	c.appendOperator(POperatorSymbol(new OperatorSymbol(atoms::MINUS)));
	c.appendSymbol(PConstantScalar(new ConstantScalar(5)));
	c.appendOperator(POperatorSymbol(new OperatorSymbol(atoms::BRACKET_RIGHT)));
	c.appendOperator(POperatorSymbol(new OperatorSymbol(atoms::MUL)));
	c.appendSymbol(PConstantScalar(new ConstantScalar(2)));

	BOOST_CHECK_EQUAL(c.calculate()->getFloatValue(), 4);
}

void CalculatorTest::calculateTestTwo() {
	Calculator c;

	c.appendSymbol(PConstantScalar(new ConstantScalar(1)));
	c.appendOperator(POperatorSymbol(new OperatorSymbol(atoms::MUL)));
	c.appendSymbol(PConstantScalar(new ConstantScalar(8)));
	c.appendOperator(POperatorSymbol(new OperatorSymbol(atoms::MINUS)));
	c.appendOperator(POperatorSymbol(new OperatorSymbol(atoms::UNARY_MINUS)));
	c.appendSymbol(PConstantScalar(new ConstantScalar(2)));

	BOOST_CHECK_EQUAL(c.calculate()->getFloatValue(), 10);
}
