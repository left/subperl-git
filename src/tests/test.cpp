#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test_framework.hpp>

#include "CalculatorTest.hpp"
#include "RegexpTest.hpp"

using namespace boost::unit_test;

test_suite* init_unit_test_suite(int argc, char** argv) {

    test_suite* suite(BOOST_TEST_SUITE("Subperl test suite"));

    boost::shared_ptr<CalculatorTest> calc(new CalculatorTest());
    boost::shared_ptr<RegexpTest> regexp(new RegexpTest());

    suite->add(BOOST_CLASS_TEST_CASE(&CalculatorTest::calculateTestOne, calc));
	suite->add(BOOST_CLASS_TEST_CASE(&CalculatorTest::calculateTestTwo, calc));
	suite->add(BOOST_CLASS_TEST_CASE(&RegexpTest::testPostfix, regexp));
	suite->add(BOOST_CLASS_TEST_CASE(&RegexpTest::testMatchRegexp, regexp));

    return suite;
}
