#ifndef CALCULATORTEST_HPP_
#define CALCULATORTEST_HPP_

#include "../Calculator.hpp"

/**
 * Klasa testujaca klase Calculator.
 */
class CalculatorTest {
	public:
		void calculateTestOne();
		void calculateTestTwo();
};

#endif /* CALCULATORTEST_HPP_ */
