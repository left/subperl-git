#include "RegexpTest.hpp"

#include <boost/test/unit_test.hpp>

void RegexpTest::testPostfix() {
	Regexp r;

	r.setRegexp("psy&(a|b)*&abc");
	BOOST_CHECK_EQUAL(r.getPostfixRegexp(), std::string("ps&y&ab|*&a&b&c&"));

	r.setRegexp("a(bb)+a");
	BOOST_CHECK_EQUAL(r.getPostfixRegexp(), std::string("abb&+&a&"));
}

void RegexpTest::testMatchRegexp() {
	Regexp r;

	r.setRegexp("(Ala|Ania)& &(ma|miala)(kotki)*,? (myszy )+,?(koniki)?.");

	BOOST_CHECK( r.match("Ania ma myszy myszy ,koniki.") == true);
}
