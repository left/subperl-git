#ifndef CALCULATOR_HPP_
#define CALCULATOR_HPP_

#include "symbols/ConstantScalar.hpp"
#include "symbols/Symbol.hpp"
#include "symbols/OperatorSymbol.hpp"
#include "exceptions/DivisionByZero.hpp"
#include "exceptions/RegexpException.hpp"

#include <vector>
#include <list>
#include <map>

#include <boost/unordered_map.hpp>
#include <boost/function.hpp>

/**
 * Klasa odpowiedzialna za wyliczanie wartosci wyrazen.
 *
 * Uzywa Polskiej Notacji Odwrotnej do obliczen i uwzglednienia priorytetow operatorow.
 */
class Calculator {
	private:
		typedef boost::unordered_map<atoms::Atom, int> AtomToPriorityMap;
		typedef boost::function<void()> OperatorHandlerFunctor;
		typedef boost::unordered_map<atoms::Atom, OperatorHandlerFunctor> AtomToHandlerMap;

		/** Mapa priorytetow dla operatorow. */
		AtomToPriorityMap atomToPriorityMap;
		/** Mapa procedur obslugi operatorow. */
		AtomToHandlerMap atomToHandlerMap;

		/** Lista wyjsciowa na ktora odkladny jest zapis w ONP. */
		std::list<PSymbol> outputStack;
		/** Pomocnicza struktura uzywana jako stos dla operatorow. */
		std::vector<POperatorSymbol> operatorStack;

		/** Stos do wyliczania wartosci w ONP. */
		std::vector<PSymbol> calculatorStack;

		/** Inicjalizuje mape priorytetow. */
		void initOperatorPriorityMap();
		/** Inicjalizuje mape procedur obslugi. */
		void initOperatorHandlersMap();

		/** Wywoluje procedure obslugi dla danego operatora. */
		void handleOperator(POperatorSymbol symbol);

		/* Procedury obslugi dla operatorow. */
		void assignment();
		void plus();
		void minus();
		void mul();
		void div() throw(DivisionByZero);
		void mod();
		void unaryPlus();
		void unaryMinus();
		void power();
		void less();
		void lessEqual();
		void greater();
		void greaterEqual();
		void equal();
		void notEqual();
		void regexpMatch();
		void orRelation();
		void andRelation();
		void notRelation();
		/* Koniec procedur obslugi dla operatorow. */

	public:
		/** K-tor */
		Calculator();
		/** D-tor */
		~Calculator();

		/**
		 * Dolacza symbol do wyrazenia ktore bedzie
		 * obliczal obiekt kalkulatora.
		 *
		 * @param symbol Symbol do odlozenia na wyjscie zapisu ONP.
		 */
		void appendSymbol(PSymbol symbol);

		/**
		 * Dolacza operator do klakulatora.
		 *
		 * @param symbol Operator ktory chcemy zastosowac w obliczeniach.
		 */
		void appendOperator(POperatorSymbol symbol);

		/**
		 * Wylicza wartosc wyrazania zbudowanego w kalkulatorze.
		 *
		 * @return Wskazanie na stala zawiarajca wynik.
		 */
		PConstantScalar calculate() throw(DivisionByZero, RegexpException);
};

#endif /* CALCULATOR_HPP_ */
